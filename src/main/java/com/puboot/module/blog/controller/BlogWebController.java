package com.puboot.module.blog.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.puboot.common.util.CoreConst;
import com.puboot.common.util.Pagination;
import com.puboot.exception.ArticleNotFoundException;
import com.puboot.module.admin.model.BizArticle;
import com.puboot.module.admin.model.BizCategory;
import com.puboot.module.admin.service.BizArticleService;
import com.puboot.module.admin.service.BizCategoryService;
import com.puboot.module.admin.service.BizThemeService;
import com.puboot.module.admin.vo.ArticleConditionVo;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import static com.puboot.common.util.CoreConst.THEME_PREFIX;

/**
 * CMS页面相关接口
 *
 * @author Linzhaoguan
 * @version V1.0
 * @date 2019年9月11日
 */
@Controller
@AllArgsConstructor
@Slf4j
public class BlogWebController {

    private final BizArticleService bizArticleService;
    private final BizCategoryService categoryService;
    private final BizThemeService bizThemeService;

    /**
     * 首页
     *
     * @param model
     * @param vo
     * @return
     */
    @GetMapping({"/", "/product/index/{pageNumber}"})
    public String index(@PathVariable(value = "pageNumber", required = false) Integer pageNumber,
                        ArticleConditionVo vo,
                        Model model) {
        if (CoreConst.SITE_STATIC.get()) {
            return "forward:/html/index/index.html";
        }
        if (pageNumber != null) {
            vo.setPageNumber(pageNumber);
        } else {
            model.addAttribute("sliderList", bizArticleService.sliderList());//轮播文章
        }
        model.addAttribute("pageUrl", "product/index");
        model.addAttribute("categoryId", "index");
        //判断是不是首页，若是，则只返回首页的数据
        if(vo.getCategoryId() == null){
            List<BizCategory> categoryList = categoryService.selectCategories(null);
            categoryList.forEach(item->{
                if(item.getName().equals("首页")){
                    vo.setCategoryId(item.getId());
                }
            });
        }
        loadMainPage(model, vo);
        String name = bizThemeService.selectCurrent().getName();
        return THEME_PREFIX + name.toLowerCase() + "/index";
    }

    /**
     * 分类列表
     *
     * @param categoryId
     * @param pageNumber
     * @param model
     * @return
     */
    @GetMapping({"/product/category/{categoryId}", "/product/category/{categoryId}/{pageNumber}"})
    public String category(@PathVariable("categoryId") Integer categoryId,
                           @PathVariable(value = "pageNumber", required = false) Integer pageNumber,
                           Model model) {
        //判断是不是公司概况，如果是，则跳转
        BizCategory category = categoryService.getById(categoryId);
        if(category.getName().equals("公司概况")){
            ArticleConditionVo vo = new ArticleConditionVo();
            vo.setCategoryId(categoryId);
            if (pageNumber != null) {
                vo.setPageNumber(pageNumber);
            }
            IPage<BizArticle> page = new Pagination<>(vo.getPageNumber(), vo.getPageSize());
            List<BizArticle> articleList = bizArticleService.findByCondition(page, vo);
            Integer articleId = articleList.get(articleList.size()-1).getId();
            if (CoreConst.SITE_STATIC.get()) {
                return "forward:/html/product/" + articleId + ".html";
            }
            BizArticle article = bizArticleService.selectById(articleId);
            if (article == null || CoreConst.STATUS_INVALID.equals(article.getStatus())) {
                throw new ArticleNotFoundException();
            }
            model.addAttribute("article", article);
            model.addAttribute("categoryId", article.getCategoryId());
            String name = bizThemeService.selectCurrent().getName();
            return THEME_PREFIX + name.toLowerCase() + "/product";
        }
        else {
            if (CoreConst.SITE_STATIC.get()) {
                return "forward:/html/index/category/" + (pageNumber == null ? categoryId : categoryId + "/" + pageNumber) + ".html";
            }
            ArticleConditionVo vo = new ArticleConditionVo();
            vo.setCategoryId(categoryId);
            if (pageNumber != null) {
                vo.setPageNumber(pageNumber);
            }
            model.addAttribute("pageUrl", "product/category/" + categoryId);
            model.addAttribute("categoryId", categoryId);
            loadMainPage(model, vo);
            String name = bizThemeService.selectCurrent().getName();
            return THEME_PREFIX + name.toLowerCase() + "/index";
        }
    }


    /**
     * 标签列表
     *
     * @param tagId
     * @param model
     * @return
     */
    @GetMapping({"/product/tag/{tagId}", "/product/tag/{tagId}/{pageNumber}"})
    public String tag(@PathVariable("tagId") Integer tagId,
                      @PathVariable(value = "pageNumber", required = false) Integer pageNumber,
                      Model model) {
        if (CoreConst.SITE_STATIC.get()) {
            return "forward:/html/index/tag/"+ (pageNumber == null ? tagId : tagId + "/" + pageNumber)  +".html";
        }
        ArticleConditionVo vo = new ArticleConditionVo();
        vo.setTagId(tagId);
        if (pageNumber != null) {
            vo.setPageNumber(pageNumber);
        }
        model.addAttribute("pageUrl", "product/tag/" + tagId);
        loadMainPage(model, vo);
        String name = bizThemeService.selectCurrent().getName();
        return THEME_PREFIX + name.toLowerCase() + "/index";
    }

    /**
     * 文章详情
     *
     * @param model
     * @param articleId
     * @return
     */
    @GetMapping("/product/detail/{articleId}")
    public String article(HttpServletRequest request, Model model, @PathVariable("articleId") Integer articleId) {
        if (CoreConst.SITE_STATIC.get()) {
            return "forward:/html/product/detail/" + articleId + ".html";
        }
        BizArticle article = bizArticleService.selectById(articleId);
        if (article == null || CoreConst.STATUS_INVALID.equals(article.getStatus())) {
            throw new ArticleNotFoundException();
        }
        model.addAttribute("article", article);
        model.addAttribute("categoryId", article.getCategoryId());
        String name = bizThemeService.selectCurrent().getName();
        return THEME_PREFIX + name.toLowerCase() + "/product";
    }

    /**
     * 评论
     *
     * @param model
     * @return
     */
    @GetMapping("/product/comment")
    public String comment(Model model) {
        if (CoreConst.SITE_STATIC.get()) {
            return "forward:/html/comment/comment.html";
        }
        model.addAttribute("categoryId", "comment");
        String name = bizThemeService.selectCurrent().getName();
        return THEME_PREFIX + name.toLowerCase() + "/comment";
    }

    private void loadMainPage(Model model, ArticleConditionVo vo) {
        vo.setStatus(CoreConst.STATUS_VALID);
        IPage<BizArticle> page = new Pagination<>(vo.getPageNumber(), vo.getPageSize());
        List<BizArticle> articleList = bizArticleService.findByCondition(page, vo);
        model.addAttribute("page", page);
        model.addAttribute("articleList", articleList);//文章列表
        if (vo.getCategoryId() != null) {
            BizCategory category = categoryService.getById(vo.getCategoryId());
            if (category != null) {
                model.addAttribute("categoryName", category.getName());
            }
        }
    }

}
